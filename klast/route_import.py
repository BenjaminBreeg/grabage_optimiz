__author__ = 'whisper'
from support import get_data_csv, haversine, reverse_transofm_coordinates
import MySQLdb
import json


def route_import(db_name, db_user, db_pass):
    # mydb = MySQLdb.connect(host='test.rsoo.ru',
    #                    user='sergey',
    #                    passwd='Et1Alr2BrGjYTObM',
    #                    db='sergey',
    #                    charset="utf8",
    #                    use_unicode=True)
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user=db_user,
                       passwd=db_pass,
                       db=db_name,
                       charset="utf8",
                       use_unicode=True)
    cursor = mydb.cursor()
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }
    # todo check truncate from trello
    json_data = json.dumps(json_data)
    type = "truck"
    result_data = get_data_csv("result_3/result.csv")
    insert_data = []
    cursor.execute('TRUNCATE TABLE route;')
    for k in range(0, len(result_data), 1):
        from_point = [result_data[k][4], result_data[k][5]]
        to_point = [result_data[k][10], result_data[k][11]]
        # cursor.execute('SELECT id FROM waste WHERE (lat BETWEEN %s AND %s) AND (lon BETWEEN %s AND %s)', [str(float(from_point[0]) - 0.0001), str(float(from_point[0]) + 0.0001), str(float(from_point[1]) - 0.0001), str(float(from_point[1]) + 0.0001)])
        # from_id = int(cursor.fetchone())
        from_id = 10
        # cursor.execute('SELECT id FROM waste WHERE (lat=%s) AND (lon=%s)', [str(to_point[0]), str(to_point[1])])
        # to_id = int(cursor.fetchone())
        to_id = 10
        # cursor.execute('SELECT lon, lat FROM waste WHERE id=%s', from_id)
        # from_geo_t = cursor.fetchone()
        # from_geo = "%s,%s" % (float(from_geo_t[0]), float(from_geo_t[1]))
        # from_geo = "%s,%s" % (from_point[0], from_point[1])
        from_geo = str(from_point[0]) + "," + str(from_point[1])
        # cursor.execute('SELECT lon, lat FROM waste WHERE id=%s', to_id)
        # to_geo_t = cursor.fetchone()
        # to_geo = "%s,%s" % (float(to_geo_t[0]), float(to_geo_t[1]))
        # to_geo = "%s,%s" % (to_point[0], to_point[1])
        to_geo = str(to_point[0]) + "," + str(to_point[1])
        length = haversine(float(from_point[0]), float(from_point[1]), float(to_point[0]), float(to_point[1]))
        time = length/50
        insert_data.append((type, from_id, to_id, from_geo, to_geo, length, time, json_data))
        print from_point, to_point
        cursor.execute('INSERT INTO route(type,from_id,to_id,length,time,data, line) '
                       'VALUES (%s, %s, %s, %s, %s, %s,  ST_GeomFromText("linestring( %s %s,%s %s )"))',
                       [type, from_id, to_id, length, time, json_data, float(result_data[k][5]),
                        float(result_data[k][4]), float(result_data[k][11]), float(result_data[k][10])])
    mydb.commit()
    cursor.close()


if __name__ == '__main__':
    route_import()