__author__ = 'whisper'
def task1_d(data):
    clusters_count = int(len(data)/200)
    xx = []
    yy = []
    weight = []
    for k in range(0, len(data)):
        xx.append(data[k][0])
        yy.append(data[k][1])
        weight.append(data[k][2])
    xx = np.asarray(xx, dtype=int)
    yy = np.asarray(yy, dtype=int)
    X = np.column_stack((xx, yy))
    t = time()
    kmeans = KMeans(n_clusters=clusters_count, n_jobs=5000)
    kmeans.fit(X)
    print time() - t
    z = 0
    for j in range(0, clusters_count, 1):
        cluster = X[kmeans.labels_ == j]
        fout = file(path + str(j) + ".csv", 'w')
        for k in range(0, len(cluster)):
            out = str(cluster[k][0]) + ',' + str(cluster[k][1]) + ',' + str(weight[z]) + '\n'
            z += 1
            fout.write(out)
        fout.close()