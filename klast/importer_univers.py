__author__ = 'whisper'
import MySQLdb
import json
import geojson
import csv
from support import get_data_csv, haversine


json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}
json_data = json.dumps(json_data)
mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()


def import_emitter(filename):
    csv_data = csv.reader(file(filename), delimiter=';')
    next(csv_data, None)
    data_to_insert = []
    data_count = 0
    for row in csv_data:
        try:
            name = row[0]
            address = row[1]
            mass = float(row[2])
            lat = float(row[3])
            lon = float(row[4])
            building_type = "emitter"
            cursor.execute('SELECT id FROM waste WHERE (lat=%s) AND (lon=%s)', [lat, lon])
            if cursor.fetchone():
                data_to_insert.append((building_type, name, address, mass, lat, lon, json_data))
                data_count += 1
        except Exception as e:
            print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            print("mass = %s, type = %s" % (mass, building_type))
            print("address = %s\ntype = %s\n" % (mass, building_type))
            continue
        if data_count % 100 == 0:
            cursor. executemany('INSERT INTO waste(type,name,address,mass,lat,lon, data)' \
                    'VALUES( %s, %s,%s,"%s","%s","%s", %s)', data_to_insert)
            data_to_insert = []
            print "%s items was imported" % data_count
    if len(data_to_insert) > 0:
        cursor. executemany('INSERT INTO waste(type,name,address,mass,lat,lon, data)' \
                        'VALUES( %s, %s,%s,"%s","%s","%s", %s)', data_to_insert)
        print "%s items was imported" % data_count
    mydb.commit()
    print "Done"


def import_route():
    route_type = "truck"
    result_data = get_data_csv("result_3/result.csv")
    count = 0
    for k in range(0, len(result_data), 1):
        count += 1
        from_point = [result_data[k][4], result_data[k][5]]
        to_point = [result_data[k][10], result_data[k][11]]
        from_id = 10
        to_id = 10
        length = haversine(float(from_point[0]), float(from_point[1]), float(to_point[0]), float(to_point[1]))
        time = length/50
        cursor.execute('INSERT INTO route(type,from_id,to_id,length,time,data, line) '
                       'VALUES (%s, %s, %s, %s, %s, %s,  ST_GeomFromText("linestring( %s %s,%s %s )"))',
                       [route_type, from_id, to_id, length, time, json_data, float(result_data[k][5]),
                        float(result_data[k][4]), float(result_data[k][11]), float(result_data[k][10])])
        print "There are %s routes in database\n" % count
    mydb.commit()
    print "Done"


def import_region():
    with open("adm.geojson") as adm:
        geodata = geojson.load(adm)
    count = 0
    for feature in geodata['features']:
        if int(feature['properties']['ADM4_ID']) == -51490:
            name = feature['properties']['NAME']
            data_type = "area"
            mass = float(feature['properties']['AREA'])
            cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                           'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                           [data_type, unicode(name), json_data, mass, feature])
            count += 1
            print "There are %s regions in database" % count
    mydb.commit()
    print "Done"

# todo create methods for true truncate tables

# todo make it work with celery
if __name__ == '__main__':
    #todo some using with switch of modes, exm if - elseif - else
    cursor.close()