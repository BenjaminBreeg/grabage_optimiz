__author__ = 'whisper'
import MySQLdb
import json
from support import get_data_csv
import geojson
import shapely.geometry
import shapely.ops

with open("adm.geojson") as adm:
    geodata = json.load(adm)
k = 0
geometry = []
mass = 0
data_type = "region"
name = "Moskow"
for feature in geodata['features']:
    if int(feature['properties']['ADM4_ID']) == -51490:
        # data_type = "area"
        mass += float(feature['properties']['AREA'])
        geometry.append(shapely.geometry.asShape(feature['geometry']))
        k += 1
merged = geometry[0]
for k in range(1, len(geometry), 1):
    merged = merged.union(geometry[k])

mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
query_s = "SELECT id, lat, lon FROM waste WHERE type='emitter';"
cursor.execute(query_s)
result = cursor.fetchall()
data = []
points = []
weaks_id = []
for k in range(0, len(result), 1):
    if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
        out = [float(result[k][1]), float(result[k][2]), int(result[k][0])]
        data.append(out)
        points.append(shapely.geometry.Point(out[1], out[0]))
print merged.contains(points[1]), data[1][2]
fout = file("weak_points_id.csv", 'w+')
for k in range(0, len(points), 1):
    if merged.contains(points[k]) == False:
        print data[k][2]
        weaks_id.append(data[k][2])
        fout.write(str(data[k][2]) + "\n")
print len(weaks_id)
fout.close()
print "Done"