__author__ = 'whisper'
# -*- coding: utf-8 -*-
import json
import shapely.geometry
import shapely.ops
import bokeh.plotting as bp

with open("adm.geojson") as adm:
    geodata = json.load(adm)
k = 0
geometry = []
id_ar = 0
z1 = [50,1,30,14,24,25,28,72,23,21,49,34,27,39,4,11,22]
zone1 = set(z1)
zone1_feature = []
zone1_mass = 0

z2 = [20,40,59,66,58,55,12,37,54,65,41,35,8,38,70,9,42,63,18,36,48,52]
zone2 = set(z2)
zone2_feature = []
zone2_mass = 0

z3 = [16, 56, 32, 44, 64, 31, 2,26,71,62,29,45,53,47,43,61,7,33,60,68,5,46,15,67,19,17,10,6]
zone3 = set(z3)
zone3_feature = []
zone3_mass = 0

data_type = "zone"
zone1_name = "Zone1"
zone2_name = "Zone2"
zone3_name = "Zone3"
for feature in geodata['features']:
    if int(feature['properties']['ADM4_ID']) == -51490:
        id_ar += 1
        if id_ar in zone1:
            zone1_feature.append(shapely.geometry.asShape(feature['geometry']))
            zone1_mass += float(feature['properties']['AREA'])
        if id_ar in zone2:
            zone2_feature.append(shapely.geometry.asShape(feature['geometry']))
            zone2_mass += float(feature['properties']['AREA'])
        if id_ar in zone3:
            zone3_feature.append(shapely.geometry.asShape(feature['geometry']))
            zone3_mass += float(feature['properties']['AREA'])

zone1_merged = zone1_feature[0]
for k in range(1, len(zone1_feature), 1):
    zone1_merged = zone1_merged.union(zone1_feature[k])

zone2_merged = zone2_feature[0]
for k in range(1, len(zone2_feature), 1):
    zone2_merged = zone2_merged.union(zone2_feature[k])

zone3_merged = zone3_feature[0]
for k in range(1, len(zone3_feature), 1):
    zone3_merged = zone3_merged.union(zone3_feature[k])

# print zone1_merged, "\n"
# print zone2_merged, "\n"
# print zone3_merged, "\n"
bp.output_file("zones.html")
p = bp.figure(plot_width=600, plot_height=600, title="Zones")
area_id = 0
step = 100
for feature in geodata['features']:
    if int(feature['properties']['ADM4_ID']) == -51490:
        area_id += 1
        name = feature['properties']['NAME']
        shape_g = shapely.geometry.asShape(feature['geometry'])
        # for k in range(0, len(shape_g), 1):
        if len(shape_g.exterior.coords) > 0:
            text_x = []
            text_y = []
            for j in range(0, len(shape_g.exterior.coords), step):
                text_x.append(shape_g.exterior.coords[j][0])
                text_y.append(shape_g.exterior.coords[j][1])
            print str(area_id)

            p.text(text_x, text_y, text=[str(area_id)], text_color="firebrick", text_align="center", text_font_size="10pt")

# print len(zone1_merged)
# print list(zone1_merged[0].interiors)
zone1_merged = shapely.geometry.MultiPolygon([zone1_merged])
zone2_merged = shapely.geometry.MultiPolygon([zone2_merged])
zones = [zone1_merged, zone2_merged, zone3_merged]
colors = ['black', 'blue', 'green', 'red']
colorset = 1
for zone in zones:
    # for k in range(0, len(zone), 1):
    if colorset == 4 or colorset == 5:
        if len(zone.exterior.coords) > 0:
            print "lol"
            ext_line_x = []
            ext_line_y = []
            # for zone_m in zone.geoms:
            for j in range(0, len(zone.exterior.coords), 1):
                ext_line_x.append(zone.exterior.coords[j][0])
                ext_line_y.append(zone.exterior.coords[j][1])
            p.line(ext_line_x, ext_line_y, line_color=colors[colorset])
    else:
        print "lol %s" %  len(zone)
        for k in range(0, len(zone), 1):
            ext_line_x = []
            ext_line_y = []
            for j in range(0, len(zone[k].exterior.coords), 1):
                ext_line_x.append(zone[k].exterior.coords[j][0])
                ext_line_y.append(zone[k].exterior.coords[j][1])
            p.line(ext_line_x, ext_line_y, line_color=colors[colorset])
            print "lol %s" %  colorset
    colorset += 1
bp.save(p)
print "Done"