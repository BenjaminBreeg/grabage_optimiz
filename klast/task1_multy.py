__author__ = 'whisper'
import csv
from sklearn.cluster import Birch
import numpy as np
from time import time
from multiprocessing import Pool
from math import sqrt
from support import translate_geo_to_decart
import MySQLdb
path = "result_1/"
threshold = 10000
source_sel = "snt"

def clusterize(x):
    birch = Birch(n_clusters=None, threshold=threshold)
    birch.fit(x)
    buf = []
    for j in range(0, len(birch.subcluster_centers_), 1):
        cluster = x[birch.labels_ == j]
        buf.append(cluster)
    return buf


def get_latlon_by_point(point, data):
    for k in range(0, len(data)):
        if float(data[k][2]) == float(point[0]) and float(data[k][3]) == float(point[1]):
            return [data[k][0], data[k][1]]


def task1_db(path_to):
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user='sergey',
                           passwd='Et1Alr2BrGjYTObM',
                           db='sergey',
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    query_s = "SELECT id, mass, lat, lon FROM waste WHERE (lat BETWEEN %s AND %s) AND (lon BETWEEN %s AND %s);" % (54.24, 56.97, 35.13, 40.18)
    cursor.execute(query_s)
    result = cursor.fetchall()
    data = []
    k_oth = 0.0000436*365
    # xx = []
    # yy = []
    # weight = []
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][2]), float(result[k][3]), float(result[k][1])*k_oth, int(result[k][0])]
            data.append(out)
    print len(data)
    data_n = translate_geo_to_decart(data)
    data_b = []
    data_g = translate_geo_to_decart(data)
    for k in range(0, len(data)):
        out = [data[k][0], data[k][1], data_g[k][0], data_g[k][1]]
        data_b.append(out)
    # data = get_data_csv(path_to)
    xx = []
    yy = []
    weight = []
    o_id = []
    minx = float(data_n[0][0])
    maxx = float(data_n[0][0])
    for k in range(1, len(data), 1):
        if maxx < float(data[k][0]):
            maxx = float(data[k][0])
        if minx > float(data[k][0]):
            minx = float(data[k][0])
    global threshold
    threshold = int((maxx-minx)/sqrt(50))
    lat = []
    lon = []
    for k in range(0, len(data_n), 1):
        xx.append(data_n[k][0])
        yy.append(data_n[k][1])
        weight.append(data[k][2])
        o_id.append(data[k][3])
    xx = np.asarray(xx, dtype='float64')
    yy = np.asarray(yy, dtype='float64')
    # weight = np.asarray(weight, dtype='float')
    o_id = np.asarray(o_id, dtype='int')
    # lat = np.asarray(lat, dtype='float64')
    # lon = np.asarray(lon, dtype='float64')
    # todo roll outside of array to search id
    X = np.column_stack((xx, yy))
    z = []
    divider = 3
    for k in range(0, divider, 1):
        if k < divider - 1:
            z.append(X[(len(X)/divider)*k: (len(X)/divider)*(k+1)])
        else:
            z.append(X[(len(X)/divider)*k:])
    pool = Pool(64)
    result = pool.map(clusterize, z)
    w = 0
    filenum = 0
    for j in range(0, divider, 1):
        items = result[j]
        for k in range(0, len(items), 1):
            fout = file(path + str(filenum) + ".csv", 'w')
            filenum += 1
            item = items[k]
            for i in range(0, len(item)):
                geo_point = get_latlon_by_point([float(item[i][0]), float(item[i][1])], data_b)
                # geo_point = [0,0]
                out = str(item[i][0]) + ',' + str(item[i][1]) + ',' + str(weight[w]) + ',' + str(geo_point[0]) + ',' + str(geo_point[1]) + '\n'
                w += 1
                fout.write(out)
            fout.close()


def get_data_from_csv():
    # data_school = csv.reader(file('school.csv'), delimiter=';')
    # data_snt = csv.reader(file('snt.csv'), delimiter=';')
    # data_mosgaz = csv.reader(file('mosoblgaz.csv'), delimiter=';')
    if source_sel == "gkn":
        data_gkn = csv.reader(file('gkn.csv'), delimiter=';')
    else:
        data_gkn = csv.reader(file('snt.csv'), delimiter=';')
    data = []
    count = 1
    k_oth = 0.0000436*365
    for row in data_gkn:
        try:
            if source_sel == "gkn":
                mass = float(row[3])*k_oth
                lat = float(row[7])
                lon = float(row[6])
            else:
                mass = float(row[2].replace(',', '.'))*k_oth
                lat = float(row[3].replace(',', '.'))
                lon = float(row[4].replace(',', '.'))
            data.append([lat, lon, mass, count])
            count += 1
        except:
            print "Cannot get geodata on %s object" % (count + 1)
    return data


def task1_csv():
    data = get_data_from_csv()
    # print len(data)
    data_n = translate_geo_to_decart(data)
    data_b = []
    data_g = translate_geo_to_decart(data)
    for k in range(0, len(data)):
        out = [data[k][0], data[k][1], data_g[k][0], data_g[k][1]]
        data_b.append(out)
    # data = get_data_csv(path_to)
    xx = []
    yy = []
    weight = []
    o_id = []
    minx = float(data_n[0][0])
    maxx = float(data_n[0][0])
    for k in range(1, len(data), 1):
        if maxx < float(data[k][0]):
            maxx = float(data[k][0])
        if minx > float(data[k][0]):
            minx = float(data[k][0])
    global threshold
    threshold = int((maxx-minx)/sqrt(70))
    for k in range(0, len(data_n), 1):
        xx.append(data_n[k][0])
        yy.append(data_n[k][1])
        weight.append(data[k][2])
    xx = np.asarray(xx, dtype='float64')
    yy = np.asarray(yy, dtype='float64')
    X = np.column_stack((xx, yy))
    z = []
    divider = 3
    for k in range(0, divider, 1):
        if k < divider - 1:
            z.append(X[(len(X)/divider)*k: (len(X)/divider)*(k+1)])
        else:
            z.append(X[(len(X)/divider)*k:])
    pool = Pool(64)
    result = pool.map(clusterize, z)
    w = 0
    filenum = 0
    for j in range(0, divider, 1):
        items = result[j]
        for k in range(0, len(items), 1):
            fout = file(path + str(filenum) + ".csv", 'w')
            filenum += 1
            item = items[k]
            for i in range(0, len(item)):
                geo_point = get_latlon_by_point([float(item[i][0]), float(item[i][1])], data_b)
                out = str(item[i][0]) + ',' + str(item[i][1]) + ',' + str(weight[w]) + ',' + str(geo_point[0]) + ',' + str(geo_point[1]) + '\n'
                w += 1
                fout.write(out)
            fout.close()


if __name__ == '__main__':
    path = "add_kps_01.csv"
    task1_db(path)