# -*- coding: utf-8 -*-
__author__ = 'whisper'
from support import get_data_csv, translate_geo_to_decart, prepare_dirs
from extrapolation import extrapolation
import MySQLdb


def parse_data(path):
    data = get_data_csv(path)
    buf = []
    for k in range(0, len(data), 1):
        buf.append([float(data[k][0]), float(data[k][1]), float(data[k][2])])
    return buf


def prepare_geo(path_to_file):
    # k_oth = 0.0000436*365
    # data = parse_data(path_from_file)
    # for k in range(0, len(data), 1):
    #     data[k][0] = float(data[k][0])
    #     data[k][1] = float(data[k][1])
    #     data[k][2] = float(data[k][2])*k_oth
    # data = translate_geo_to_decart(data)
    # for k in range(0, len(data), 1):
    #     data[k][0] = int(round(data[k][0]))
    #     data[k][1] = int(round(data[k][1]))

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user='sergey',
                           passwd='Et1Alr2BrGjYTObM',
                           db='sergey',
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    query_s = "SELECT id, mass, lat, lon FROM waste WHERE (lat BETWEEN %s AND %s) AND (lon BETWEEN %s AND %s) AND (type ='emitter');" % (54.24, 56.97, 35.13, 40.18)
    cursor.execute(query_s)
    result = cursor.fetchall()
    data = []
    k_oth = 0.0000436*365
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][2]), float(result[k][3]), float(result[k][1])*k_oth, int(result[k][0])]
            data.append(out)
    data2 = translate_geo_to_decart(data)
    for k in range(0, len(data), 1):
        data2[k][0] = int(round(data2[k][0]))
        data2[k][1] = int(round(data2[k][1]))
    print data[0], data2[0]
    fout = file(path_to_file, 'w+')
    for k in range(0, len(data), 1):
        out = str(data2[k][0]) + "," + str(data2[k][1]) + "," + str(data2[k][2]) + "," + str(data[k][0]) + "," + str(data[k][1]) + "," + str(data[k][3]) + '\n'
        fout.write(out)
    fout.close()


# todo compose this with dataset :3
def prepare_data(path):
    prepare_dirs()
    prepare_geo(path)
    extrapolation(path, "prepared_data2.csv")


if __name__ == '__main__':
    prepare_data("prepared_data.csv")
