__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import itertools
import json
from support import get_data_csv

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}
json_data = json.dumps(json_data)
# mydb = MySQLdb.connect(host='localhost',
#                        user='root',
#                        passwd='12345',
#                        db='rsso_gkn',
#                        charset="utf8",
#                        use_unicode=True)
mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
csv_data = csv.reader(file('gkn.csv'), delimiter=';')
regions = get_data_csv("regions_number_vs_name.csv")
next(csv_data, None)  # skip the header
count = 0

# cursor.execute('TRUNCATE TABLE waste;')
insert_data = []
for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        try:
            region_id_t = row[20].split(" ")
            for k in range(0, len(region_id_t), 1):
                for j in range(0, len(regions), 1):
                    if region_id_t[k] == regions[j][1]:
                        region_id = regions[j][0]
        except:
            region_id = 0
        name = row[2]
        address = row[5]
        mass = float(row[3])
        lat = float(row[7])
        lon = float(row[6])
        building_type = "emitter"
        insert_data.append((building_type, name, address, mass, lat, lon, json_data, region_id))
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass,building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
# print insert_data[0], insert_data[1], insert_data[5000]
    cursor.execute('INSERT INTO waste(type, name, address, mass, lat, lon, data, region_id) VALUES( %s, %s,%s,"%s","%s","%s", %s, %s)',[building_type, name, address, mass, lat, lon, json_data, region_id])
    # if region_id != " ":
    #     cursor.execute('INSERT INTO waste(type,name,address,mass,lat,lon, data, region_id)' \
    #      'VALUES( %s, %s,%s,"%s","%s","%s", %s, %s)', [building_type, name, address, mass, lat, lon, json_data, region_id])
    # else:
    #     cursor.execute('INSERT INTO waste(type,name,address,mass,lat,lon, data)' \
    #      'VALUES( %s, %s,%s,"%s","%s","%s", %s)', [building_type, name, address, mass, lat, lon, json_data])

print "--- there are %s in gkn.csv" % count
mydb.commit()
cursor.close()
print "Done"