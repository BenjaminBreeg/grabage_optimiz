__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import json
from support import get_data_csv
from terr_schem import terr_schem
from prepare_kp_start import prepare_data
from route_import import route_import
from region_import import area_import
from region_zone_import import zone_import

db_user = 'test'
db_name = 'test'
db_pass = 'Pc_09#245iG'

# db_user = 'sergey'
# db_name = 'sergey'
# db_pass = 'Et1Alr2BrGjYTObM'


def sort_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_user,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('infrastructure_sorting.csv'), delimiter=';')
    # next(csv_data, None)  # skip the header
    count = 0
    insert_data_1 = []
    id_buf = []
    db_name = 'waste'
    truncate_query = """
    LOCK TABLES %s WRITE;
    DELETE FROM %s;
    ALTER TABLE %s auto_increment=1;
    UNLOCK TABLES;
    """ % (db_name,db_name,db_name)
    truncate_query_list = truncate_query.strip().split('\n')
    # cursor.execute('TRUNCATE TABLE waste')
    for line in truncate_query_list:
            cursor.execute(line)
    # fout = file("infrastructure_placing_t.csv", 'w+')
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = row[1]
            address = str(row[2]) + " " + str(row[3])
            try:
                mass = float(row[12])
            except:
                mass = 1000
            lat = float(row[4])
            lon = float(row[5])
            # print mass
            building_type = "infrastructure"
            category = "sorting"
        except Exception as e:
            print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            print("mass = %s, type = %s" % (mass,building_type))
            print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, parent_id, grandparent_id, data, point)' \
        'VALUES( %s, %s,%s,%s,"%s",%s, %s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, 0, 0, json_data, lon, lat])
        # cursor.execute('SELECT id FROM waste WHERE lat=%s', [float(lat)])
        # l_id = cursor.fetchone()
        # print l_id
        # out = [row, l_id]
        # fout.write(str(out[0]) + "," + str(out[1]))
        # fout.write("\n")
    # fout.close()
    print "--- there are %s in sorting.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


def placing_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('infrastructure_placing.csv'), delimiter=';')
    # next(csv_data, None)  # skip the header
    count = 0
    insert_data_1 = []
    id_buf = []
    # cursor.execute('TRUNCATE TABLE waste')
    # fout = file("infrastructure_placing_t.csv", 'w+')
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = row[1]
            address = str(row[2]) + " " + str(row[3])
            try:
                mass = float(row[12])
            except:
                mass = 1000
            lat = float(row[4])
            lon = float(row[5])
            # print mass
            building_type = "infrastructure"
            category = "placing"
        except Exception as e:
            print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            print("mass = %s, type = %s" % (mass,building_type))
            print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, parent_id, grandparent_id, data, point)' \
        'VALUES( %s, %s,%s,%s,"%s",%s, %s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, 0, 0, json_data, lon, lat])
        # cursor.execute('SELECT id FROM waste WHERE lat=%s', [float(lat)])
        # l_id = cursor.fetchone()
        # print l_id
        # out = [row, l_id]
        # fout.write(str(out[0]) + "," + str(out[1]))
        # fout.write("\n")
    # fout.close()
    print "--- there are %s in placing.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


def container_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }
    json_data = json.dumps(json_data)
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    # mydb = MySQLdb.connect(host='localhost',
    #                        user='root',
    #                        passwd='12345',
    #                        db='rsso_gkn',
    #                        charset="utf8",
    #                        use_unicode=True)
    cursor = mydb.cursor()
    csv_data = get_data_csv("containers_s.csv")
    count = 0
    insert_data = []
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = " "
            address = " "
            mass = float(row[2])
            lat = float(row[0])
            lon = float(row[1])
            building_type = "container"
            category = "container"
            grandparent_lat = float(row[10])
            grandparent_lon = float(row[11])
            grandparent_id = int(row[9])
            # cursor.execute('SELECT id FROM waste WHERE (lat=%s) AND (lon=%s) AND (type="infrastructure")', [grandparent_lat, grandparent_lon])
            # if cursor.fetchone() != None:
            #     grandparent_id = int(cursor.fetchone())
            #     print grandparent_id
            # else:
            #     grandparent_id = 0
        except Exception as e:
            print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            print("mass = %s, type = %s" % (mass, building_type))
            print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,address,category,mass, parent_id, grandparent_id, data, point)' \
        'VALUES( %s, %s,%s,%s,"%s",%s, %s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, address, category, mass, grandparent_id, 0, json_data, lon, lat])
    print "--- there are %s in containers.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


def school_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('school.csv'), delimiter=';')
    kp = get_data_csv("containers_s.csv")
    count = 0
    category = "school"
    next(csv_data, None)  # skip the header
    count = 0
    insert_data = []
    query_s = "SELECT id, lat, lon, parent_id FROM waste WHERE type='container';"
    cursor.execute(query_s)
    result = cursor.fetchall()
    kp_data = []
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][1]), float(result[k][2]), int(result[k][0]), int(result[k][3])]
            kp_data.append(out)
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = unicode(row[0], 'cp1251')
            address = unicode(row[1], 'cp1251')
            mass = float(row[2].replace(',', '.'))
            lat = float(row[3].replace(',', '.'))
            lon = float(row[4].replace(',', '.'))

            for k in range(0, len(kp), 1):
                if round(lat, 4) == round(float(kp[k][5]), 4) and round(lon, 4) == round(float(kp[k][6]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])
                    # print "lol1"
                if round(lat, 4) == round(float(kp[k][7]), 4) and round(lon, 4) == round(float(kp[k][8]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])

            for k in range(0, len(kp_data), 1):
                if round(parent_lat, 4) == kp_data[k][0] and round(parent_lon, 4) == kp_data[k][1]:
                    parent_id = kp_data[k][2]
                    grandparent_id = kp_data[k][3]
            # parent_id = cursor.fetchall()[0]
            # grandparent_id = cursor.fetchall()[1]
            # if parent_id == None:
            #     parent_id = 0
            #     grandparent_id = 0
            # else:
            #     parent_id = int(parent_id)
            #     grandparent_id = int(grandparent_id)
            building_type = "emitter"
            # insert_data.append((building_type, name, address, mass, lat, lon, json_data))
        except Exception as e:
            # print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            # print("mass = %s, type = %s" % (mass, building_type))
            # print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, data, parent_id, grandparent_id, point)' \
         'VALUES( %s, %s,%s,%s,"%s",%s,%s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, json_data, parent_id, grandparent_id, lon, lat])
    print "--- there are %s in school.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


def snt_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('snt.csv'), delimiter=';')
    kp = get_data_csv("containers_s.csv")
    count = 0
    next(csv_data, None)  # skip the header
    count = 0
    category = "snt"
    insert_data = []
    query_s = "SELECT id, lat, lon, parent_id FROM waste WHERE type='container';"
    cursor.execute(query_s)
    result = cursor.fetchall()
    kp_data = []
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][1]), float(result[k][2]), int(result[k][0]), int(result[k][3])]
            kp_data.append(out)
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = unicode(row[0], 'cp1251')
            address = unicode(row[1], 'cp1251')
            mass = float(row[2].replace(',', '.'))
            lat = float(row[3].replace(',', '.'))
            lon = float(row[4].replace(',', '.'))

            for k in range(0, len(kp), 1):
                if round(lat, 4) == round(float(kp[k][5]), 4) and round(lon, 4) == round(float(kp[k][6]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])
                    # print "lol1"
                if round(lat, 4) == round(float(kp[k][7]), 4) and round(lon, 4) == round(float(kp[k][8]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])

            for k in range(0, len(kp_data), 1):
                if round(parent_lat, 4) == kp_data[k][0] and round(parent_lon, 4) == kp_data[k][1]:
                    parent_id = kp_data[k][2]
                    grandparent_id = kp_data[k][3]
            # parent_id = cursor.fetchall()[0]
            # grandparent_id = cursor.fetchall()[1]
            # print parent_id
            # if parent_id == None:
            #     parent_id = 0
            #     grandparent_id = 0
            # else:
            #     parent_id = int(parent_id)
            #     grandparent_id = int(grandparent_id)
            building_type = "emitter"
            # insert_data.append((building_type, name, address, mass, lat, lon, json_data))
        except Exception as e:
            # print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            # print("mass = %s, type = %s" % (mass, building_type))
            # print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, data, parent_id, grandparent_id, point)' \
         'VALUES( %s, %s,%s,%s,"%s",%s,%s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, json_data, parent_id, grandparent_id, lon, lat])
    print "--- there are %s in snt.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


def mosgaz_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('mosoblgaz.csv'), delimiter=';')
    kp = get_data_csv("containers_s.csv")
    count = 0
    next(csv_data, None)  # skip the header
    count = 0
    category = "mosoblgaz"
    insert_data = []
    query_s = "SELECT id, lat, lon, parent_id FROM waste WHERE type='container';"
    cursor.execute(query_s)
    result = cursor.fetchall()
    kp_data = []
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][1]), float(result[k][2]), int(result[k][0]), int(result[k][3])]
            kp_data.append(out)
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
        try:
            name = row[0]
            address = row[1]
            mass = row[2]
            lat = row[3]
            lon = row[4]

            for k in range(0, len(kp), 1):
                if round(lat, 4) == round(float(kp[k][5]), 4) and round(lon, 4) == round(float(kp[k][6]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])
                    # print "lol1"
                if round(lat, 4) == round(float(kp[k][7]), 4) and round(lon, 4) == round(float(kp[k][8]), 4):
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])

            for k in range(0, len(kp_data), 1):
                if round(parent_lat, 4) == kp_data[k][0] and round(parent_lon, 4) == kp_data[k][1]:
                    parent_id = kp_data[k][2]
                    grandparent_id = kp_data[k][3]
            # parent_id = cursor.fetchall()[0]
            # grandparent_id = cursor.fetchall()[1]
            print "lol"
            # if parent_id == None:
            #     parent_id = 0
            #     grandparent_id = 0
            # else:
            #     parent_id = int(parent_id)
            #     grandparent_id = int(grandparent_id)
            building_type = "emitter"
            # insert_data.append((building_type, name, address, mass, lat, lon, json_data))
        except Exception as e:
            # print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            # print("mass = %s, type = %s" % (mass, building_type))
            # print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, data, parent_id, grandparent_id, point)' \
         'VALUES( %s,%s,%s,%s,"%s",%s,%s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, json_data, parent_id, grandparent_id, lon, lat])
    print "--- there are %s in mosgaz.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"

def gkn_import():
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }

    json_data = json.dumps(json_data)

    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    csv_data = csv.reader(file('gkn.csv'), delimiter=';')
    kp = get_data_csv("containers_s.csv")
    next(csv_data, None)  # skip the header
    count = 0
    category = "gkn"
    query_s = "SELECT id, lat, lon, parent_id FROM waste WHERE type='container';"
    cursor.execute(query_s)
    result = cursor.fetchall()
    kp_data = []
    parent_id = 0
    grandparent_id = 0
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][1]), float(result[k][2]), int(result[k][0]), int(result[k][3])]
            kp_data.append(out)
    for row in csv_data:
        count += 1
        if count % 1e2 == 0:
            print(count)
            mydb.commit()
        try:
            name = row[2]
            address = row[5]
            mass = float(row[3])
            lat = float(row[7])
            lon = float(row[6])
            for k in range(0, len(kp), 1):
                if round(lat, 3) <= round(float(kp[k][5])+0.1, 4) and round(lat, 3) >= round(float(kp[k][5])-0.1, 4) and round(lon, 3) <= round(float(kp[k][6]), 3)+0.1 and round(lon, 4) >= round(float(kp[k][6]), 2)-0.01:
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])
                if round(lat, 3) <= round(float(kp[k][7])+0.1, 4) and round(lat, 3) >= round(float(kp[k][7])-0.1, 4) and round(lon, 3) <= round(float(kp[k][8]), 3)+0.1 and round(lon, 4) >= round(float(kp[k][8]), 2)-0.01:
                    parent_lat = float(kp[k][0])
                    parent_lon = float(kp[k][1])

            for k in range(0, len(kp_data), 1):
                if round(parent_lat, 4) == kp_data[k][0] and round(parent_lon, 4) == kp_data[k][1]:
                    parent_id = kp_data[k][2]
                    grandparent_id = kp_data[k][3]
            building_type = "emitter"
            # print parent_id, grandparent_id, mass
            # cursor.execute('INSERT INTO waste(type,name,address,mass, data, parent_id, grandparent_id, point)' \
            # 'VALUES( %s, %s,%s,"%s",%s,%s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, address, mass, json_data, parent_id, grandparent_id, lon, lat])

        except Exception as e:
            # print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
            # print("mass = %s, type = %s" % (mass, building_type))
            # print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
            continue
        cursor.execute('INSERT INTO waste(type,name,category,address,mass, data, parent_id, grandparent_id, point)' \
        'VALUES( %s,%s,%s,%s,"%s",%s,%s, %s,ST_GeomFromText("point( %s %s )"))',[building_type, name, category, address, mass, json_data, parent_id, grandparent_id, lon, lat])
    print "--- there are %s in gkn.csv" % count
    mydb.commit()
    cursor.close()
    print "Done"


if __name__ == '__main__':
    print "imports infrastructure"
    sort_import()
    placing_import()
    print "preparing container data"
    prepare_data("prepared_data.csv")
    print "executing terr_schem"
    terr_schem()
    print "containers import"
    container_import()
    print "emitters import"
    snt_import()
    # mosgaz_import()
    # gkn_import()
    print "import route.truck"
    route_import(db_name=db_name, db_user=db_user, db_pass=db_pass)
    print "import region.area"
    area_import(db_name=db_name, db_user=db_user, db_pass=db_pass)
    print "import region.zone"
    zone_import(db_name=db_name, db_user=db_user, db_pass=db_pass)
    print "All done"