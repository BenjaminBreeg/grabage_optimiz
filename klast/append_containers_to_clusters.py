__author__ = 'whisper'
from support import get_data_csv
from os import listdir


def append_containers_to_cluster():
    containers = get_data_csv("containers.csv")
    buf = []
    cluster_count = len(listdir("result_1"))
    filenames = range(0, cluster_count, 1)
    fout = file("containers_d.csv", 'w+')
    t = 0
    for filename in filenames:
        path = "result_1/" + str(filename) + ".csv"
        cluster = get_data_csv(path)
        if len(cluster) > 1:
            for j in range(1, len(cluster), 1):
                for k in range(0, len(containers), 1):
                    try:
                        if float(containers[k][5]) == float(cluster[j][3]) and float(containers[k][6]) == float(cluster[j][4]):

                                buf.append([containers[k][0], containers[k][1], containers[k][2], containers[k][3], containers[k][4], containers[k][5], containers[k][6], containers[k][7], containers[k][8], filename])
                                out = str(buf[t][0]) + "," + str(buf[t][1]) + "," + str(buf[t][2]) + "," + str(buf[t][3]) + "," + str(buf[t][4]) + "," + str(buf[t][5]) + "," + str(buf[t][6]) + "," + str(buf[t][7]) + "," + str(buf[t][8]) + "," + str(buf[t][9]) + "\n"
                                t += 1
                                fout.write(out)
                                break
                    except:
                        print "Some troubles in %s cluster" % k
                        break
    fout.close()
    fout = file("containers_s.csv", 'w+')
    cont = get_data_csv("containers_d.csv")
    infr = get_data_csv("result_3/objects.csv")
    for k in range(0, len(cont), 1):
        for j in range(0, len(infr), 1):
            if int(cont[k][9]) == int(infr[j][4]):
                out = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (cont[k][0], cont[k][1], cont[k][2], cont[k][3], cont[k][4], cont[k][5], cont[k][6], cont[k][7], cont[k][8], cont[k][9], infr[j][5], infr[j][6])
                fout.write(out)
    fout.close()