__author__ = 'whisper'
from support import get_data_csv


data = get_data_csv("nodes.csv")
fout = file("nodes_true.csv", 'w+')
for k in range(0, len(data), 1):
    out = "%s,%s,%s\n" % (data[k][1], data[k][0], data[k][2])
    fout.write(out)
fout.close()