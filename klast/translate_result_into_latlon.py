__author__ = 'whisper'
from support import get_data_csv, reverse_transofm_coordinates

buf = []
data = get_data_csv("result_3/result.csv")
for k in range(0, len(data), 1):
    start_p = reverse_transofm_coordinates([data[k][1], data[k][0]])
    end_p = reverse_transofm_coordinates([data[k][4], data[k][3]])
    out = [start_p[0], start_p[1], data[k][2], end_p[0], end_p[1], data[k][5], data[k][6], data[k][7], data[k][8]]
    buf.append(out)
fout = file("result_3/result_mod.csv", 'w+')
for k in range(0, len(buf), 1):
    out = str(buf[k][0]) + "," + str(buf[k][1]) + "," + str(buf[k][2]) + "," + str(buf[k][3]) + "," + str(buf[k][4]) + "," + str(buf[k][5]) + "," + str(buf[k][6]) + "," + str(buf[k][7]) + "," + str(buf[k][8]) + "\n"
    fout.write(out)
fout.close()