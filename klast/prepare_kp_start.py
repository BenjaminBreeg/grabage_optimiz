__author__ = 'whisper'
# -*- coding: utf-8 -*-
from support import get_data_csv, translate_geo_to_decart, prepare_dirs
from extrapolation import extrapolation
import MySQLdb
import csv

source_sel = "snt"

def parse_data(path):
    data = get_data_csv(path)
    buf = []
    for k in range(0, len(data), 1):
        buf.append([float(data[k][0]), float(data[k][1]), float(data[k][2])])
    return buf


def get_data_from_csv():
    # data_school = csv.reader(file('school.csv'), delimiter=';')
    # data_snt = csv.reader(file('snt.csv'), delimiter=';')
    # data_mosgaz = csv.reader(file('mosoblgaz.csv'), delimiter=';')
    if source_sel == "gkn":
        data_gkn = csv.reader(file('gkn.csv'), delimiter=';')
    else:
        data_gkn = csv.reader(file('snt.csv'), delimiter=';')
    data = []
    count = 1
    k_oth = 0.0000436*365
    for row in data_gkn:
        try:
            if source_sel == "gkn":
                mass = float(row[3])*k_oth
                lat = float(row[7])
                lon = float(row[6])
            else:
                mass = float(row[2].replace(',', '.'))*k_oth
                lat = float(row[3].replace(',', '.'))
                lon = float(row[4].replace(',', '.'))
            data.append([lat, lon, mass, count])
            count += 1
        except:
            print "Cannot get geodata on %s object" % (count + 1)
    return data


def prepare_geo(path_to_file):
    # k_oth = 0.0000436*365
    # data = parse_data(path_from_file)
    # for k in range(0, len(data), 1):
    #     data[k][0] = float(data[k][0])
    #     data[k][1] = float(data[k][1])
    #     data[k][2] = float(data[k][2])*k_oth
    # data = translate_geo_to_decart(data)
    # for k in range(0, len(data), 1):
    #     data[k][0] = int(round(data[k][0]))
    #     data[k][1] = int(round(data[k][1]))

    # mydb = MySQLdb.connect(host='test.rsoo.ru',
    #                        user='sergey',
    #                        passwd='Et1Alr2BrGjYTObM',
    #                        db='sergey',
    #                        charset="utf8",
    #                        use_unicode=True)
    # cursor = mydb.cursor()
    # query_s = "SELECT id, mass, lat, lon FROM waste WHERE (lat BETWEEN %s AND %s) AND (lon BETWEEN %s AND %s) AND (type ='emitter');" % (54.24, 56.97, 35.13, 40.18)
    # cursor.execute(query_s)
    # result = cursor.fetchall()
    # data = []
    # k_oth = 0.0000436*365
    # for k in range(0, len(result), 1):
    #     if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
    #         out = [float(result[k][2]), float(result[k][3]), float(result[k][1])*k_oth, int(result[k][0])]
    #         data.append(out)
    data = get_data_from_csv()
    data2 = translate_geo_to_decart(data)
    for k in range(0, len(data), 1):
        data2[k][0] = int(round(data2[k][0]))
        data2[k][1] = int(round(data2[k][1]))
    fout = file(path_to_file, 'w+')
    for k in range(0, len(data), 1):
        out = str(data2[k][0]) + "," + str(data2[k][1]) + "," + str(data2[k][2]) + "," + str(data[k][0]) + "," + str(data[k][1]) + "," + str(data[k][3]) + '\n'
        fout.write(out)
    fout.close()


# todo compose this with dataset :3
def prepare_data(path):
    prepare_dirs()
    prepare_geo(path)
    extrapolation(path, "containers.csv")



if __name__ == '__main__':
    prepare_data("prepared_data.csv")
