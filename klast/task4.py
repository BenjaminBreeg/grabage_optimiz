__author__ = 'whisper'
import csv
from math import sqrt
from random import random
from multiprocessing import Pool
from time import time
from support import get_data_csv, haversine, get_real_data, get_distance
path = "result_3/"
first = []
second = []
third = []


def square_dist(start, end):
    return (float(end[0]) - float(start[0]))**2 + (float(end[1]) - float(start[1]))**2


def target_func(third_d, second_d, first_d):
    # k3 = 0.5 + random()
    # k2 = 0.5 + random()
    # k1 = 0.5 + random()
    k1 = 1.6
    k2 = 1.6
    k3 = 1.3
    dist32 = get_distance(third_d, second_d)
    dist21 = get_distance(second_d, first_d)
    k3 *= int(dist21 + dist32)/2
    k2 *= int(dist21 + dist32)/2
    k1 *= int(dist21 + dist32)/2
    center_weigth = float(third_d[2])
    return center_weigth*k3*(dist32+k2*k2+k2*dist21+k2*k1)


def item_to_str(item):
    return str(item[0]) + ',' + str(item[1]) + ',' + str(item[2]) + ',' + str(item[3]) + ',' + str(item[5]) + ',' + str(item[6])


def seek_better(third_d):
    result_first = first[0]
    result_second = second[0]
    target = target_func(third_d, result_second, result_first)
    for k in range(0, len(second), 1):
        for j in range(0, len(first), 1):
            target_n = target_func(third_d, second[k], first[j])
            if target_n < target:
                    target = target_n
                    result_second = second[k]
                    result_first = first[j]
    buf = [target, item_to_str(result_first), item_to_str(result_second), item_to_str(third_d), result_first[2] +
           result_second[2] + third_d[2]]
    return buf


def target_func_short(second_d, first_d):
    # k1 = 0.5 + random()
    k1 = 1.6
    # k2 = 0.5 + random()
    k2 = 1.6
    dist21 = get_distance(second_d, first_d)
    k1 *= dist21
    k2 *= dist21
    center_weigth = float(second_d[2])
    return center_weigth*k2*dist21 + center_weigth*k2*k1


def seek_better_short(second_d):
    target = target_func_short(second_d, first[0])
    result_first = first[0]
    for k in range(0, len(first), 1):
        target_n = target_func_short(second_d, first[k])
        if target_n < target:
            target = target_n
            result_first = first[k]
    buf = [target, item_to_str(result_first), item_to_str(second_d), result_first[2] + second_d[2]]
    return buf


def task4():
    items = get_data_csv(str(path) + "objects.csv")
    for k in range(0, len(items), 1):
        if int(items[k][2]) == 1:
            global first
            first.append(items[k])
        if int(items[k][2]) == 2:
            global second
            second.append(items[k])
        if int(items[k][2]) == 3:
            global third
            third.append(items[k])
    pool = Pool(4)
    print len(first), len(second), len(third)
    if len(second) > 0:
        results = pool.map(seek_better, third)
    else:
        results = []
    results2 = pool.map(seek_better_short, third)
    path_out = path + "result.csv"
    fout = file(path_out, 'w+')
    for item in results:
        if int(item[0]) > -1:
            out = str(item[1]) + ',' + str(item[2]) + ',' + str(item[3]) + ',' + str(item[0]) + '\n'
            fout.write(out)

    for item in results2:
        if int(item[0]) > -1:
            out = str(item[1]) + ',' + str(item[2]) + ',' + str(item[0]) + '\n'
            fout.write(out)
    fout.close()


if __name__ == '__main__':
    task4()