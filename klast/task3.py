__author__ = 'whisper'
from os import listdir
from math import sqrt
from random import random
from multiprocessing import Pool
import MySQLdb
from time import time
from support import get_data_csv, get_distance, get_real_data, translate_geo_to_decart
path = "result_3/"
path_from = "result_1/"
real = []


def dist(x1, x2, y1, y2):
    return sqrt((x2 - x1)**2 + (y2 - y1)**2)


def target_func(weigth, distance):
    # k = 0.5 + random()
    k = 1.6
    val = weigth*distance + weigth*(k*distance)
    return val


def compuze_better(filename):
    id_file = filename
    filename = path_from + str(filename) + ".csv"
    items = get_data_csv(filename)
    mass_cent = items[0]
    if float(mass_cent[0]) > 0.0 and float(mass_cent[1]) > 0.0:
        real[1]
        target_point = real[1]
        for k in range(0, len(real), 1):
            if get_distance(mass_cent, real[k]) > get_distance(mass_cent, target_point):
                target_point = real[k]
        weight = 0
        for k in range(1, len(items), 1):
            weight += float(items[k][2])
        for k in range(1, len(real), 1):
            if int(real[k][2]) > 0:
                if target_func(weight, get_distance(mass_cent, target_point)) >\
                        target_func(weight, get_distance(mass_cent, real[k])):
                    target_point = real[k]
        for k in range(1, len(real), 1):
            if float(real[k][0]) == float(target_point[0]) and \
                    float(real[k][1]) == float(target_point[1]) and \
                    float(real[k][2]) == float(target_point[2]):
                global real
                real[k] = [0, 0, 0, 0, 0]
                break
        buf = [target_point[0], target_point[1], int(target_point[2]), weight, id_file, target_point[3], target_point[4]]
        return buf
    else:
        return [-1, -1, -1, -1, -1, -1]


def task3_slow():
    cluster_count = len(listdir(path_from))
    filenames = range(0, cluster_count, 1)
    global real
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user='test',
                           passwd='Pc_09#245iG',
                           db='test',
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    query_s = "SELECT id, lat, lon FROM waste WHERE type='infrastructure';"
    cursor.execute(query_s)
    result = cursor.fetchall()
    data_g = []
    k_oth = 0.0000436*365
    # xx = []
    # yy = []
    # weight = []
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            if k < 31:
                reg_id = 3
            else:
                reg_id = 1
            out = [float(result[k][1]), float(result[k][2]), reg_id]
            data_g.append(out)
    # real = get_real_data("nodes_true.csv")
    data_b = translate_geo_to_decart(data_g)
    real = []
    for k in range(0, len(data_g), 1):
        real.append([data_b[k][0], data_b[k][1], data_b[k][2], data_g[k][0], data_g[k][1]])
    result = []
    for k in range(0, cluster_count, 1):
        result.append(compuze_better(filenames[k]))
    path_out = path + "objects.csv"
    fout = file(path_out, 'w')
    for item in result:
        if float(item[0]) > 0:
            out = str(item[0]) + ',' + str(item[1]) + ',' + str(item[2]) + ',' + str(item[3]) + ',' + str(item[4]) + ',' + str(item[5]) + ',' + str(item[6]) + '\n'
            fout.write(out)
    fout.close()


if __name__ == '__main__':
    t = time()
    task3_slow()