__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import json

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}

json_data = json.dumps(json_data)

mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
csv_data = csv.reader(file('infrastructure_sorting.csv'), delimiter=';')
# next(csv_data, None)  # skip the header
count = 0
insert_data_1 = []
id_buf = []
db_name = 'geodata'
truncate_query = """
LOCK TABLES %s WRITE;
DELETE FROM %s;
ALTER TABLE %s auto_increment=1;
UNLOCK TABLES;
""" % (db_name,db_name,db_name)
truncate_query_list = truncate_query.strip().split('\n')
# cursor.execute('TRUNCATE TABLE waste')
# for line in truncate_query_list:
#         cursor.execute(line)
fout = file("infrastructure_placing_t.csv", 'w+')
for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = row[1]
        address = str(row[2]) + " " + str(row[3])
        try:
            mass = float(row[12])
        except:
            mass = 1000
        lat = float(row[4])
        lon = float(row[5])
        print mass
        building_type = "infrastructure"
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass,building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
    cursor.execute('INSERT INTO waste(type,name,address,mass, parent_id, grandparent_id, data, point)' \
    'VALUES( %s, %s,%s,"%s",%s, %s, %s,ST_GeomFromText("point( %s,%s )"))',[building_type, name, address, mass, 0, 0, json_data, lon, lat])
    # cursor.execute('SELECT id FROM waste WHERE lat=%s', [float(lat)])
    # l_id = cursor.fetchone()
    # print l_id
    # out = [row, l_id]
    # fout.write(str(out[0]) + "," + str(out[1]))
    # fout.write("\n")
# fout.close()
print "--- there are %s in sorting.csv" % count
mydb.commit()
cursor.close()
print "Done"