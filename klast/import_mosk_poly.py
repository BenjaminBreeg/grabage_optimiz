__author__ = 'whisper'
import MySQLdb
import json
from support import get_data_csv
import geojson
import shapely.geometry
import shapely.ops

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}
mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
json_data = json.dumps(json_data)
# data = get_data_csv("regions_number_vs_name.csv")
with open("adm.geojson") as adm:
    geodata = json.load(adm)
k = 0
geometry = []
mass = 0
data_type = "region"
name = "Moskow"
for feature in geodata['features']:
    if int(feature['properties']['ADM4_ID']) == -51490:
        # data_type = "area"
        mass += float(feature['properties']['AREA'])
        geometry.append(shapely.geometry.asShape(feature['geometry']))
        k += 1
merged = geometry[0]
for k in range(1, len(geometry), 1):
    merged = merged.union(geometry[k])
merged_geo = geojson.Feature(geometry=merged, properties={})
print merged_geo
# cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
#                        'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
#                        [data_type, name, json_data, mass, merged_geo])
# mydb.commit()
# cursor.close()
print "Done"