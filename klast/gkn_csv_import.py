__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import json

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}

json_data = json.dumps(json_data)

mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
csv_data = csv.reader(file('gkn.csv'), delimiter=';')
next(csv_data, None)
count = 0

for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = row[0]
        address = row[1]
        mass = row[2]
        lat = row[3]
        lon = row[4]
        building_type = "emitter"
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass,building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
    cursor.execute('INSERT INTO waste(type,name,address,mass, data, point)' \
     'VALUES( %s, %s,%s,"%s","%s","%s", %s,ST_GeomFromText("point( %s %s )"))', [building_type, name, address, mass, lat, lon, json_data, lon, lat])
    if count == 10:
        break
mydb.commit()
cursor.close()
print "Done"