__author__ = 'whisper'
# -*- coding: utf-8 -*-
import MySQLdb
import json
import geojson
import shapely.geometry
import shapely.ops


def zone_import(db_name, db_user, db_pass):
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    json_data = json.dumps(json_data)
    # data = get_data_csv("regions_number_vs_name.csv")
    with open("adm.geojson") as adm:
        geodata = json.load(adm)
    k = 0
    geometry = []
    id_ar = 0
    z1 = [50,1,30,14,24,25,28,72,23,21,49,34,27,39,4,11,22]
    zone1 = set(z1)
    zone1_feature = []
    zone1_mass = 0

    z2 = [20,40,59,66,58,55,12,37,54,65,41,35,8,38,70,9,42,63,18,36,48,52]
    zone2 = set(z2)
    zone2_feature = []
    zone2_mass = 0

    z3 = [16, 56, 32, 44, 64, 31, 2,26,71,62,29,45,53,47,43,61,7,33,60,68,5,46,15,67,19,17,10,6]
    zone3 = set(z3)
    zone3_feature = []
    zone3_mass = 0

    data_type = "zone"
    zone1_name = "Zone1"
    zone2_name = "Zone2"
    zone3_name = "Zone3"
    for feature in geodata['features']:
        if int(feature['properties']['ADM4_ID']) == -51490:
            id_ar += 1
            if id_ar in zone1:
                zone1_feature.append(shapely.geometry.asShape(feature['geometry']))
                zone1_mass += float(feature['properties']['AREA'])
            if id_ar in zone2:
                zone2_feature.append(shapely.geometry.asShape(feature['geometry']))
                zone2_mass += float(feature['properties']['AREA'])
            if id_ar in zone3:
                zone3_feature.append(shapely.geometry.asShape(feature['geometry']))
                zone3_mass += float(feature['properties']['AREA'])
    zone1_merged = zone1_feature[0]
    for k in range(1, len(zone1_feature), 1):
        zone1_merged = zone1_merged.union(zone1_feature[k])
    zone1_merged = shapely.geometry.MultiPolygon([zone1_merged])
    zone1_merged_t = geojson.Feature(geometry=zone1_merged, properties={})

    zone2_merged = zone2_feature[0]
    for k in range(1, len(zone2_feature), 1):
        zone2_merged = zone2_merged.union(zone2_feature[k])
    zone2_merged = shapely.geometry.MultiPolygon([zone2_merged])
    zone2_merged_t = geojson.Feature(geometry=zone2_merged, properties={})

    zone3_merged = zone3_feature[0]
    for k in range(1, len(zone3_feature), 1):
        zone3_merged = zone3_merged.union(zone3_feature[k])
    zone3_merged_t = geojson.Feature(geometry=zone3_merged, properties={})
    # print zone1_merged_t, zone2_merged_t, zone3_merged_t

    cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                           'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                           [data_type, zone1_name, json_data, zone1_mass, zone1_merged_t])
    print "Zone 1 was imported"
    cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                           'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                           [data_type, zone2_name, json_data, zone2_mass, zone2_merged_t])
    print "Zone 2 was imported"
    cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                           'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                           [data_type, zone3_name, json_data, zone3_mass, zone3_merged_t])
    print "Zone 3 was imported"
    mydb.commit()
    cursor.close()
    print "Done"