__author__ = 'whisper'
# -*- coding: utf-8 -*-
import MySQLdb
import json
from support import get_data_csv
import geojson
import shapely.geometry


def area_import(db_name, db_user, db_pass):
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                           user=db_user,
                           passwd=db_pass,
                           db=db_name,
                           charset="utf8",
                           use_unicode=True)
    cursor = mydb.cursor()
    cursor.execute('TRUNCATE TABLE region')
    json_data = json.dumps(json_data)
    # data = get_data_csv("regions_number_vs_name.csv")
    with open("adm.geojson") as adm:
        geodata = geojson.load(adm)
    k = 0
    geometry = []
    ext_geom = []
    ext_mass = 0
    ext_name = "Балашиха"
    ext_type = "area"
    for feature in geodata['features']:
        if int(feature['properties']['ADM4_ID']) == -51490:
            if k == 19 or k == 68:
                ext_mass += float(feature['properties']['AREA'])
                ext_geom.append(shapely.geometry.asShape(feature['geometry']))
            else:
                name = feature['properties']['NAME']
                data_type = "area"
                mass = float(feature['properties']['AREA'])
                geometry = feature['geometry']
                cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                               'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                               [data_type, unicode(name), json_data, mass, feature])
            # print feature
            # break
            k += 1
            print "There are %s regions in database" % k
    if len(ext_geom) > 0:
        ext_geom = ext_geom[0].union(ext_geom[1])
        ext_geom = geojson.Feature(geometry=ext_geom, properties={})
        cursor.execute('INSERT INTO region(type, name, data, mass, poly) '
                               'VALUES (%s, %s, %s, %s, ST_GeomFromGeoJSON(%s))',
                               [ext_type, ext_name, json_data, ext_mass, ext_geom])
    mydb.commit()
    cursor.close()
    print "Done"