__author__ = 'whisper'
from support import get_data_csv, get_distance, get_node
from multiprocessing import Pool
from os import listdir
path_to_res2 = "result_1/"


def calculate_nvv(data):
    first_type = []
    second_type = []
    third_type = []
    first_type.append(get_node(1, data[0]))
    if get_node(2, data[0]):
        second_type.append(get_node(2, data[0]))
    third_type.append(get_node(3, data[0]))
    is_unique = True
    for k in range(0, len(data), 1):
        for j in range(0, len(first_type), 1):
            if float(get_node(1, data[k])[0]) == float(first_type[j][0]) and float(get_node(1, data[k])[1]) == float(first_type[j][1]):
                is_unique = False
        if is_unique is True:
            first_type.append(get_node(1, data[k]))
        is_unique = True
    is_unique = True
    for k in range(0, len(data), 1):
        if get_node(2, data[k]):
            for j in range(0, len(second_type), 1):
                if float(get_node(2, data[k])[0]) == float(second_type[j][0]) and float(get_node(2, data[k])[1]) == float(second_type[j][1]):
                    is_unique = False
            if is_unique is True:
                second_type.append(get_node(2, data[k]))
        is_unique = True
    is_unique = True
    for k in range(0, len(data), 1):
        if get_node(3, data[k]):
            for j in range(0, len(third_type), 1):
                if float(get_node(3, data[k])[0]) == float(third_type[j][0]) and float(get_node(3, data[k])[1]) == float(third_type[j][1]):
                    is_unique = False
            if is_unique is True:
                third_type.append(get_node(3, data[k]))
        is_unique = True
    first_mass = []
    for k in range(0, len(first_type)-1, 1):
        sum = float(first_type[k][2])
        for j in range(0, len(data)):
            if float(first_type[k][0]) == float(get_node(1, data[j])[0]) and float(first_type[k][1]) == float(get_node(1, data[j])[1]):
                sum += float(get_node(1, data[j])[2])
        first_mass.append(sum)
    second_mass = []
    for k in range(0, len(second_type), 1):
        sum = float(second_type[k][2])
        for j in range(0, len(data)):
            if float(second_type[k][0]) == float(get_node(2, data[j])[0]) and float(second_type[k][1]) == float(get_node(2, data[j])[1]):
                sum += float(get_node(2, data[j])[2])
        second_mass.append(sum)
    third_mass = []
    for k in range(0, len(third_type), 1):
        sum = float(third_type[k][2])
        for j in range(0, len(data)):
            if get_node(3, data[j]):
                if float(third_type[k][0]) == float(get_node(3, data[j])[0]) and float(third_type[k][1]) == float(get_node(3, data[j])[1]):
                    sum += float(get_node(3, data[j])[2])
        third_mass.append(sum)
    nvv_ground = 0
    for k in range(0, len(first_mass), 1):
        nvv_ground += float(first_mass[k])
    nvv_fire = 0
    for k in range(0, len(second_mass), 1):
        nvv_fire += float(second_mass[k])
    nvv_sort = 0
    for k in range(0, len(third_mass), 1):
        nvv_sort += float(third_mass[k])
    nvv_fire = (nvv_fire*3500)/1000000
    nvv_ground = (nvv_ground*682)/1000000
    nvv_sort = (nvv_sort*420)/1000000
    return nvv_ground + nvv_fire + nvv_sort


def read_zone(filename):
    path = str(path_to_res2) + str(filename) + ".csv"
    data_z = get_data_csv(path)
    weight = 0
    for k in range(1, len(data_z), 1):
        weight += float(data_z[k][2])
    return float(data_z[0][2])*float(weight)

def calculate_tkm_zone():
    zones_count = len(listdir(path_to_res2)) - 3
    filenames = range(0, zones_count, 1)
    pool = Pool(16)
    result = pool.map(read_zone, filenames)
    tkm_zone = 0
    for k in range(0, len(result), 1):
        tkm_zone += result[k]
    return tkm_zone


def calculate_tkm(data):
    tkm32 = 0
    tkm21 = 0
    tkm31 = 0
    for k in range(0, len(data), 1):
        if get_node(3, data[k]):
            tkm32 += get_distance(get_node(3, data[k]), get_node(2, data[k]))*float(get_node(3, data[k])[2])
    for k in range(0, len(data), 1):
        tkm21 += get_distance(get_node(2, data[k]), get_node(1, data[k]))*float(get_node(2, data[k])[2])
    for k in range(0, len(data), 1):
        if get_node(3, data[k]):
            tkm31 += get_distance(get_node(3, data[k]), get_node(1, data[k]))*float(get_node(3, data[k])[2])
    tkm = 1.6*(tkm32 + tkm21 + tkm31)
    tkm_zone = calculate_tkm_zone()
    tkm += tkm_zone
    return tkm


def disk_nvv():
    # t = time()
    csv_file = "result_3/result.csv"
    data = get_data_csv(csv_file)
    nvv = calculate_nvv(data)
    tkm = calculate_tkm(data)
    base = nvv + tkm
    D = 0.1
    for k in range(1, 15, 1):
        base += base/((1-D)**k)
    return base
    # print time()-t


if __name__ == '__main__':
    disk_nvv()