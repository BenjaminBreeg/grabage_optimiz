__author__ = 'whisper'
from math import sqrt
import csv
from multiprocessing import Pool
from os import listdir
path = "result_3/"
import time
import bokeh.plotting as bp


def get_data(path):
    csvf = open(path, 'r')
    rows = csv.reader(csvf)
    buf = [row for row in rows]
    csvf.close()
    return buf


def dist(x1, x2, y1, y2):
    return sqrt((int(x2) - int(x1))**2 + (int(y2) - int(y1))**2)#


def plot(objdata="", routesdata="", **kwargs):
    name_str = kwargs.get('name',"")
    bp.output_file("clusters_cms"+name_str+".html")
    p = bp.figure(plot_width=600, plot_height=600,title=kwargs.get('title',u''))
    colorset = kwargs.get('colors',['green','magenta','SandyBrown' ,'red','blue','DarkGreen','pink','cyan','grey','orange','brown','yellow'])
    if len(routesdata)!=0:
        for i, route in enumerate(routesdata):
            # print(routesdata[i])
            the_color = colorset[i%len(colorset)]
            x,y = [], []
            print len(routesdata[1])
            for idx in range(0,len(routesdata[i])-4, 6):
                x.append(float(routesdata[i][idx+4]))
                y.append(float(routesdata[i][idx+5]))
                node_class = routesdata[i][idx+2]
                print("x=%d, y=%d" % (x[-1], y[-1]))
                # if len(x)>1:
                p.line(x[-2:], y[-2:], line_width=4.-idx, line_color=the_color, line_alpha = 1.)
                if float(node_class) == 1:
                    p.triangle(x[-1], y[-1], fill_color=the_color, size=7., line_color='black', fill_alpha=0.5)
                if float(node_class) == 2:
                    p.square(x[-1], y[-1], fill_color=the_color, size=7., line_color='black', fill_alpha=0.5)
                if float(node_class) == 3:
                    p.circle(x[-1], y[-1], fill_color=the_color, size=7., line_color='black', fill_alpha=0.5)

    if len(objdata)!=0:
        for i, data in enumerate(objdata):
            if len(data)==0:
                break
            # print(objdata[i])
            the_color = colorset[i%len(colorset)]
            # p.square(float(objdata[i][0]), float(objdata[i][1]), fill_color=the_color, size=3., line_color='black', fill_alpha=1)
            x = float(objdata[i][5])
            y = float(objdata[i][6])
            # mass = float(objdata[i][2])
            node_type = float(objdata[i][2])
            if float(node_type) == 1:
                p.triangle(x, y, fill_color=the_color, size=3., line_color='black', fill_alpha=1)
            if float(node_type) == 2:
                p.square(x, y, fill_color=the_color, size=3., line_color='black', fill_alpha=1)
            if float(node_type) == 3:
                p.circle(x, y, fill_color=the_color, size=3., line_color='black', fill_alpha=1)
    bp.save(p)


def plot_objects_and_routes():

    obj_data = get_data(path + "objects.csv")
    routes_data = get_data(path + "result.csv")
    plot(obj_data, routes_data)


def plot_nodes():

    obj_data = get_data("nodes_true.csv")
    plot(obj_data)


if __name__ == '__main__':
    start_time = time.time()
    plot_objects_and_routes()
    print("--- %s seconds for plot_objects_and_routes() ---" % (time.time() - start_time))
    # plot_nodes()
    # print("--- %s seconds for plot_nodes() ---" % (time.time() - start_time))