__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import json
from support import get_data_csv

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}

json_data = json.dumps(json_data)

mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()
csv_data = csv.reader(file('snt.csv'), delimiter=';')
kp = get_data_csv("containers_s.csv")
next(csv_data, None)  # skip the header
count = 0
insert_data_1 = []
for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = unicode(row[0], 'cp1251')
        address = unicode(row[1], 'cp1251')
        mass = float(row[2].replace(',', '.'))
        lat = float(row[3].replace(',', '.'))
        lon = float(row[4].replace(',', '.'))
        parent_lat = 0.0
        parent_lon = 0.0
        for k in range(0, len(kp), 1):
            if lat == float(kp[k][5]) and lon == float(kp[k][6]):
                parent_lat = float(kp[k][0])
                parent_lon = float(kp[k][1])
            if lat == float(kp[k][7]) and lon == float(kp[k][8]):
                parent_lat = float(kp[k][0])
                parent_lon = float(kp[k][1])
        cursor.execute('SELECT id, parent_id FROM waste WHERE (lat=%s) AND (lon=%s)', [parent_lat, parent_lon])
        parent_id = cursor.fetchone()[0]
        grandparent_id = cursor.fetchone()[1]
        print "lol"
        if parent_id == None:
            parent_id = 0
            grandparent_id = 0
        else:
            parent_id = int(parent_id)
            grandparent_id = int(grandparent_id)
        building_type = "emitter"
        # insert_data_1.append((building_type, name, address, mass, lat, lon, json_data))
    except Exception as e:
        # print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        # print("mass = %s, type = %s" % (mass,building_type))
        # print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
    cursor.execute('INSERT INTO waste(type,name,address,mass, data, parent_id, grandparent_id, point)' \
     'VALUES( %s, %s,%s,"%s","%s","%s", %s)',[building_type, name, address, mass, json_data, parent_id, grandparent_id, lon, lat])

print "--- there are %s in snt.csv" % count
mydb.commit()
cursor.close()
print "Done"
