__author__ = 'whisper'
from math import sqrt
import csv
from multiprocessing import Pool
from os import listdir
path = "data/"
import time
import bokeh.plotting as bp

def get_data(path):
    csvf = open(path, 'r')
    rows = csv.reader(csvf)
    buf = [row for row in rows]
    csvf.close()
    return buf


def dist(x1, x2, y1, y2):
    return sqrt((int(x2) - int(x1))**2 + (int(y2) - int(y1))**2)#

def plot(arr_n_items, objdata, **kwargs):
    name_str = kwargs.get('name',"")
    bp.output_file("clusters_cms"+name_str+".html")
    p = bp.figure(plot_width=600, plot_height=600,title=kwargs.get('title',u''))
    colorset = kwargs.get('colors',['green','red','blue','magenta','grey','yellow'])
    for i, data in enumerate(arr_n_items):
        print("%d from %d" % (i, len(arr_n_items)))
        x = data[0]
        y = data[1]
        x_kps = data[4][0]
        y_kps = data[4][1]
        the_color = colorset[i%len(colorset)]
        p.circle(x, y, fill_color=the_color, size=6, line_color=the_color, fill_alpha=0.3)
        p.circle(x_kps, y_kps, fill_color=the_color, size=2, line_color=the_color, fill_alpha=0.3)
        print("i = %d, len = %d" % (i,len(objdata)))
        p.circle(float(objdata[0][i]), float(objdata[1][i]), fill_color=the_color, size=14., line_color=the_color, fill_alpha=1)
        # print(objdata[0][i])
        # print(objdata[1][i])
        # print(objdata[2][i])

    bp.show(p)

def plot_clusters():
    cluster_count =len(listdir(path))-1
    filenames = range(0, cluster_count, 1)
    clusters_data = []
    colorset = ['green','red','blue','magenta','grey','yelow']
    for fname in filenames:
        fname = path + str(fname) + ".csv"
        cluster_items = get_data(fname)
        cluster_params_strs = cluster_items[0]
        cluster_params = [float(param_str) for param_str in cluster_params_strs] # + len(cluster_items)
        cluster_params.append(len(cluster_items)-1)
        cluster_params.append(list(map(list, zip(*cluster_items[1:]))))
        clusters_data.append(cluster_params)
    # print(obj_data[0][:10])
    # print(obj_data[1][:10])
    # print(obj_data[2][:10])

    obj_data = get_data(path + "objects.csv")
    obj_data = list(map(list, zip(*obj_data)))

    imax=10
    step = 1
    length = len(obj_data[0])
    # plot(clusters_data[:imax],obj_data[:imax],name="_0 %d" % imax)

    # obj_data = get_data(path + "objects.csv")[0::step]
    obj_data = get_data(path + "objects.csv")
    obj_data = list(map(list, zip(*obj_data)))
    plot(clusters_data[0::step],obj_data,name="step %d" % step)

    # obj_data = get_data(path + "objects.csv")[length-imax:]
    # obj_data = list(map(list, zip(*obj_data)))
    # plot(clusters_data[length-imax:],obj_data,name="_len-%d len" % imax)

if __name__ == '__main__':
    start_time = time.time()
    plot_clusters()
    print("--- %s seconds for plot_clusters() ---" % (time.time() - start_time))