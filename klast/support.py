# -*- coding: cp1251 -*-
__author__ = 'whisper'
import csv
import numpy as np
from math import radians, cos, sin, asin, sqrt, degrees
from openpyxl import load_workbook
import MySQLdb
from os import makedirs, chmod



def get_data_csv(path):
    """
    :param path: full path to .csv file
    :return: dict by lines
    """
    csvf = open(path, 'r')
    rows = csv.reader(csvf)
    buf = [row for row in rows]
    csvf.close()
    return buf


def perform_data_birch(x, y, divider=-1):
    """
    :param x: X-axe dict
    :param y: Y-axe dict
    :param divider: parts count for multiprocessing
    :return: performed dict of dicts
    """
    x = np.asarray(x, dtype=float)
    y = np.asarray(y, dtype=float)
    X = np.column_stack((x, y))
    z = []
    if divider > 0:
        for k in range(0, divider, 1):
            if k < divider - 1:
                z.append(X[(len(X)/divider)*k: (len(X)/divider)*(k+1)])
            else:
                z.append(X[(len(X)/divider)*k:])
    else:
        z.append(X)
    return z


def manage_clusters_array(data, divider):
    """
    :param data: array of cluster`s arrays
    :param divider: parts count for multiprocessing
    :return: arrays of clusters
    """
    buf = []
    for j in range(0, divider, 1):
        items = data[j]
        for k in range(0, len(items), 1):
            buf.append(items[k])
    # print buf[0]
    return buf


def get_distance(start, end):
    """
    calculate distance between two points
    :param start: start pint [x,y,args...]
    :param end: end pint [x,y,args...]
    :return: distance between start and end
    """
    return float(sqrt((float(end[0]) - float(start[0]))**2 + (float(end[1]) - float(start[1]))**2))


def get_middle_point(start, end):
    """
    :param start: first point [x,y]
    :param end:  second pint [x,y]
    :return: middle of vector [x1,y1,x2,y2]
    """
    result_x = int(round((int(start[0])+int(end[0]))/2, -1))
    result_y = int(round((int(start[1])+int(end[1]))/2, -1))

    return result_x, result_y


def get_summ(data, index):
    """
    :param data: array of dicts
    :param index: column index
    :return: summary value
    """
    summ = 0.0
    for item in data:
        summ += float(item[index])
    return summ


def execute_coordinates(xlsx_file_in, csv_file_out, end_of_table=1):
    """
    transform xslx-file to csv for columns of coordinates
    :param xlsx_file_in:
    :param csv_file_out:
    :param end_of_table: for what value function start count end of table
    :return: csv file
    """
    book = load_workbook(xlsx_file_in)
    sheet = book.get_sheet_by_name(book.get_sheet_names()[0])
    print sheet.cell(row=82, column=5).value
    while 1:
        if sheet.cell(row=end_of_table, column=1).value is None:
            break
        end_of_table += 1
    print end_of_table
    buf = []
    for k in range(2, end_of_table, 1):
        tmp = [sheet.cell(row=k, column=6).value, sheet.cell(row=k, column=7).value, sheet.cell(row=k, column=3).value]
        buf.append(tmp)
        print tmp
    fout = file(csv_file_out, 'w+')
    for item in buf:
        out = u"%s,%s,%s \n" % (item[0], item[1], item[2])
        fout.write(out)


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    dec_m = km * 100
    return dec_m


# todo lets find left-low point of square
def translate_geo_to_decart(data):
    """
    converts lonlat into meters
    :param data: dict of data in format [[lon, lat], ...]
    :return: converted data in format [[x, y], ...]
    """
    buf = []
    min_val = [54.24, 35.13]
    for k in range(0, len(data), 1):
        x = haversine(float(min_val[1]), float(min_val[0]), float(min_val[1]), float(data[k][0]))
        y = haversine(float(min_val[1]), float(min_val[0]), float(data[k][1]), float(min_val[0]))
        if len(data[k]) > 3:
            buf.append([x, y, float(data[k][2]), int(data[k][3])])
        else:
            buf.append([x, y, float(data[k][2])])
    return buf


# todo change it, coz on diff parallels ds1x and ds2y may be differ
def reverse_transofm_coordinates(point):
    """
    reverse translate coords into [lat, lon] from distances
    :param point: point in format [x, y]
    :return: point in format [lat lon]
    """
    RADIUS = 6367 * 100
    ps = [54.24, 35.13]
    distance = sqrt(float(point[0])**2 + float(point[1])**2)
    dlat = distance / RADIUS
    # print sin(dlat) / cos(radians(ps[1]))
    if sin(dlat) > 1:
        return [-1, -1]
    dlon = asin(sin(dlat) / cos(radians(ps[1])))
    out = [ps[0] + degrees(dlon), ps[1] + degrees(dlat)]
    return out


def get_real_data(path_to_real):
    """
    return data of real objects
    :param path_to_real: path to csv file with coordinates on WGS84 and node Id
    :return: real data in format[[x,y,id], ...]
    """
    data = get_data_csv(path_to_real)
    data_b = translate_geo_to_decart(data)
    buf = []
    for k in range(0, len(data), 1):
        buf.append([data_b[k][0], data_b[k][1], data_b[k][2], data[k][0], data[k][1]])
    return buf


def read_coordinates_from_mysql():
    """
    reads coordinates and areas from MySql database
    :return: data in format [[lon, lat, area], ...]
    """
    # todo take settings from django.settings
    db_con = MySQLdb.Connection(host='localhost',
                                user='user',
                                passwd='user',
                                db='gkn_base',
                                charset='utf8',
                                use_unicode=True)
    db_cur = db_con.cursor()
    query_s = "SELECT area, lon, lat FROM oks_adr2;"
    db_cur.execute(query_s)
    result = db_cur.fetchall()
    buf =[]
    for k in range(0, len(result), 1):
        if float(result[k][1]) != 0.0 and float(result[k][2]) != 0.0:
            out = [float(result[k][1]), float(result[k][2]), int(result[k][0])]
            buf.append(out)
    return buf


def prepare_dirs():
    try:
        makedirs("result_1")
    except:
        print "cannot create directory results_1"
    try:
        makedirs("result_3")
    except:
        print "cannot create directory  results_3"
    chmod("result_1", 0777)
    chmod("result_3", 0777)


def get_node(node_id, array):
    """
    search node in array by id
    :param node_id: node id
    :param array: searchable array
    :return: node
    """
    id_n = 0
    if node_id == 1:
        if int(array[2]) == 1:
            id_n = 0
    if node_id == 2:
        if int(array[8]) == 2:
            id_n = 6
    if node_id == 3:
        if len(array) > 13 and int(array[12]) == 3:
            id_n = 10
        elif len(array) < id_n + 4 and int(array[5]) == 3:
            id_n = 4
    if len(array) > id_n + 3:
        return [array[id_n], array[id_n+1], array[id_n+3]]