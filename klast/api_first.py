__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import itertools
import json

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}

json_data = json.dumps(json_data)

mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
cursor = mydb.cursor()

import csv

def unicode_csv_reader(utf8_data, **kwargs):
    csv_reader = csv.reader(utf8_data, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, 'cp1251') for cell in row]

csv_data = csv.reader(file('snt.csv'), delimiter=';')

next(csv_data, None)  # skip the header
count = 0
insert_data_1 = []
for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = unicode(row[0], 'cp1251')
        address = unicode(row[1], 'cp1251')
        mass = float(row[2].replace(',', '.'))
        lat = float(row[3].replace(',', '.'))
        lon = float(row[4].replace(',', '.'))
        building_type = "emitter"
        insert_data_1.append((building_type, name, address, mass, lat, lon, json_data))
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass,building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
cursor.executemany('INSERT IGNORE INTO waste(type,name,address,mass,lat,lon, data)' \
 'VALUES( %s, %s,%s,"%s","%s","%s", %s)',insert_data_1)
    # if count == 10:
    #     break

csv_data2 = csv.reader(file('mosoblgaz.csv'), delimiter=';')
next(csv_data2, None)  # skip the header
print "--- there are %s in snt.csv" % count
count = 0
insert_data_2 = []
for row in csv_data2:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = row[0]
        address = row[1]
        mass = float(row[2])
        lat = float(row[3])
        lon = float(row[4])
        building_type = "emitter"
        insert_data_2.append((building_type, name, address, mass, lat, lon, json_data))
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass,building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass,building_type))
        continue
cursor.executemany('INSERT IGNORE INTO waste(type,name,address,mass,lat,lon, data)' \
 'VALUES( %s, %s,%s,"%s","%s","%s", %s)',insert_data_2)
    # if count == 10:
    #     break

csv_data3 = csv.reader(file('school.csv'), delimiter=';')

next(csv_data3, None)  # skip the header
print "--- there are %s in mosgaz.csv" % count
count = 0
insert_data_3 = []
for row in csv_data3:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = unicode(row[0], 'cp1251')
        address = unicode(row[1], 'cp1251')
        mass = float(row[2].replace(',', '.'))
        lat = float(row[3].replace(',', '.'))
        lon = float(row[4].replace(',', '.'))
        building_type = "emitter"
        insert_data_3.append((building_type, name, address, mass, lat, lon, json_data))
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass, building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
        continue
cursor.executemany('INSERT IGNORE INTO waste(type,name,address,mass,lat,lon, data)' \
 'VALUES( %s, %s,%s,"%s","%s","%s", %s)',insert_data_3)
    # if count == 10:
    #     break
print "--- there are %s in school.csv" % count
mydb.commit()
cursor.close()
print "Done"