__author__ = 'whisper'
from math import sqrt
import csv
from multiprocessing import Pool
from os import listdir
path = "result_1/"
import time
import bokeh.plotting as bp

def get_data(path):
    csvf = open(path, 'r')
    rows = csv.reader(csvf)
    buf = [row for row in rows]
    csvf.close()
    return buf


def dist(x1, x2, y1, y2):
    return sqrt((int(x2) - int(x1))**2 + (int(y2) - int(y1))**2)#

def plot(arr_n_items, **kwargs):
    bp.output_file("clusters_cms.html")
    p = bp.figure(plot_width=600, plot_height=600,title=kwargs.get('title',u''))
    colorset = kwargs.get('colors',['green','red','blue','magenta','grey','yelow'])
    for i, data in enumerate(arr_n_items):
        # print(data)
        x = data[0]
        y = data[1]
        x_kps = data[4][0]
        y_kps = data[4][1]
        the_color = colorset[i%len(colorset)]
        p.circle(x, y, fill_color=the_color, size=6, line_color=the_color, fill_alpha=0.6)
        p.circle(x_kps, y_kps, fill_color=the_color, size=2, line_color=the_color, fill_alpha=0.3)
        # p.circle(x, y, legend = legendset[i], fill_color=colorset[i], radius = radii, size=6, line_color=colorset[i], fill_alpha=0.3)
    bp.show(p)

def plot_clusters():
    cluster_count =len(listdir(path))
    filenames = range(0, cluster_count, 1)
    clusters_data = []
    bp.output_file("clusters_cms.html")
    colorset = ['green','red','blue','magenta','grey','yelow']
    for fname in filenames:
        fname = path + str(fname) + ".csv"
        cluster_items = get_data(fname)
        cluster_params_strs = cluster_items[0]
        cluster_params = [float(param_str) for param_str in cluster_params_strs] # + len(cluster_items)
        cluster_params.append(len(cluster_items)-1)
        cluster_params.append(list(map(list, zip(*cluster_items[1:]))))
        clusters_data.append(cluster_params)
    plot(clusters_data[:100])

if __name__ == '__main__':
    start_time = time.time()
    plot_clusters()