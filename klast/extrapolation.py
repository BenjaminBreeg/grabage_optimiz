__author__ = 'whisper'
from support import get_data_csv, perform_data_birch, manage_clusters_array, get_middle_point, get_summ
from multiprocessing import Pool
from sklearn.cluster import Birch
from time import time
from math import sqrt
threshold = 100
clusters_flow = 1
xx = []
yy = []
weight = []
data = []

def parallel_low(data):
    """
    for primary multiprocessing clusterisation by BIRCH algorithm
    :param data: numpy array with performed data
    :return: clustered data
    """
    birch = Birch(n_clusters=None, threshold=threshold)
    birch.fit(data)
    buf = []
    for j in range(0, len(birch.subcluster_centers_), 1):
        cluster = data[birch.labels_ == j]
        buf.append(cluster)
    return buf


#todo perform clusters point with weight
def generate_places(clust):
    cluster_size = len(clust)
    buf = []
    if cluster_size > 0:
        if len(clust)/2 > 0:
            container_weight = float(get_summ(clust, 2))/(float(len(clust)/2))
        else:
            container_weight = float(get_summ(clust, 2))/(float(len(clust)/2)+0.1)
        for k in range(0, cluster_size-1, 2):
            lat_m = float((float(clust[k][3]) + float(clust[k+1][3]))/2)
            lon_m = float((float(clust[k][4]) + float(clust[k+1][4]))/2)
            child_id_left = clust[k][5]
            child_id_right = clust[k+1][5]
            container = [lat_m, lon_m, container_weight, child_id_left, child_id_right, clust[k][3], clust[k][4], clust[k+1][3], clust[k+1][4]]
            buf.append(container)
    return buf


def concates_clusteriz_with_origin_data(cluster):
    buf = []
    for item in cluster:
        for k in range(0, len(data), 1):
            if float(item[0]) == float(data[k][0]) and float(item[1]) == float(data[k][1]):
                buf.append(data[k])
                break
    return buf


def extrapolation(filename_in, filename_out):
    t = time()
    global data
    data = get_data_csv(filename_in)
    minx = float(data[0][0])
    maxx = float(data[0][0])
    for k in range(1, len(data), 1):
        if maxx < float(data[k][0]):
            maxx = float(data[k][0])
        if minx > float(data[k][0]):
            minx = float(data[k][0])
    global threshold
    threshold = int((maxx-minx)/sqrt(70))
    for k in range(0, len(data)):
        global xx
        xx.append(data[k][0])
        global yy
        yy.append(data[k][1])
        global weight
        weight.append(data[k][2])
    data_perf = perform_data_birch(xx, yy, clusters_flow)
    pool = Pool(64)
    cluster = pool.map(parallel_low, data_perf)
    cluster = manage_clusters_array(cluster, clusters_flow)
    cluster_ext = []
    for k in range(0, len(cluster), 1):
        print "Concates %s clusters" % k
        cluster_ext.append(concates_clusteriz_with_origin_data(cluster[k]))
    containers = pool.map(generate_places, cluster_ext)
    fout = file(filename_out, 'w')
    for container in containers:
        for item in container:
            out = str(item[0]) + ',' + str(item[1]) + ',' + str(item[2]) + ',' + str(item[3]) + ',' + str(item[4]) + ',' + str(item[5]) + ',' + str(item[6]) + ',' + str(item[7]) + ',' + str(item[8]) + '\n'
            fout.write(out)
    fout.close()
    print time() - t


if __name__ == '__main__':
    extrapolation("prepared_data.csv", "prepared_data2.csv")