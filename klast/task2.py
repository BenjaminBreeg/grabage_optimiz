__author__ = 'whisper'
from multiprocessing import Pool
from os import listdir
from time import time
from support import get_data_csv, get_distance

path = "result_1/"


def compuze_data(filename):
    filename = path + str(filename) + ".csv"
    items = get_data_csv(filename)
    length = 0
    m = 0
    mx = 0
    my = 0
    for k in range(0, len(items)-1, 1):
        length += get_distance(items[k], items[k+1])
        mx += float(items[k][0])*float(items[k][2])
        my += float(items[k][1])*float(items[k][2])
        m += float(items[k][2])
    mx /= m-0.000001
    my /= m-0.000001
    out = str(mx) + ',' + str(my) + ',' + str(length) + '\n'
    with open(filename, 'r+') as fout:
        content = fout.readlines()
        fout.seek(0)
        fout.writelines([out] + content)
        fout.close()
    return length


def task2():
    cluster_count =len(listdir(path))
    filenames = range(0, cluster_count, 1)
    pool = Pool(16)
    pool.map(compuze_data, filenames)


if __name__ == '__main__':
    t = time()
    task2()
    print time() - t