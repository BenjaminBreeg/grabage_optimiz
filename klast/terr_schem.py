__author__ = 'whisper'
from task1_multy import task1_csv
from task2 import task2
from task3 import task3_slow
from task4 import task4
from nvv_1 import disk_nvv
from time import time
from os import makedirs, chmod
from append_containers_to_clusters import append_containers_to_cluster


def terr_schem():
    try:
        makedirs("result_1")

    except:
        print "cannot create directory results_1"
    try:
        makedirs("result_3")

    except:
        print "cannot create directory  results_3"
    chmod("result_1", 0777)
    chmod("result_3", 0777)
    t = time()
    task1_csv()
    t1 = time() - t
    t = time()
    task2()
    t2 = time() - t
    t = time()
    task3_slow()
    t3 = time() - t
    t = time()
    task4()
    t4 = time() - t
    append_containers_to_cluster()
    t = time()
    nvv = disk_nvv()
    t5 = time()-t
    fout = file("out.txt", 'w+')
    out = str(t1) + "," + str(t2) + "," + str(t3) + "," + str(t4) + "," + str(t5) + "," + str(nvv) + '\n'
    fout.write(out)
    fout.close()


if __name__ == '__main__':
    terr_schem()