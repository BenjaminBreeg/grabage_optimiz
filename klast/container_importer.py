__author__ = 'whisper'
 # -*- coding: utf-8 -*-
import csv
import MySQLdb
import json
from support import get_data_csv

json_data = {
    "seasons_intensity":
    {
        "summer":"1.5",
        "automn":"0.5",
        "winter":"0.5",
        "spring":"1.5"
    },
    "density": "123",
    "morphology":
    {
        "morphology_type": "type 1",
        "morphology_id": "32123",
        "composition":
        {
            "paper": "0.12",
            "plastic": "0.2",
            "organic": "0.37",
            "other": "0.05"
        }
    }
}
json_data = json.dumps(json_data)
mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
# mydb = MySQLdb.connect(host='localhost',
#                        user='root',
#                        passwd='12345',
#                        db='rsso_gkn',
#                        charset="utf8",
#                        use_unicode=True)
cursor = mydb.cursor()
csv_data = get_data_csv("containers_s.csv")
count = 0
insert_data = []
for row in csv_data:
    count += 1
    if count % 1e2 == 0:
        print(count)
    try:
        name = " "
        address = " "
        mass = float(row[2])
        lat = float(row[0])
        lon = float(row[1])
        building_type = "container"
        grandparent_lat = float(row[10])
        grandparent_lon = float(row[11])
        grandparent_id = int(row[9])
        # cursor.execute('SELECT id FROM waste WHERE (lat=%s) AND (lon=%s) AND (type="infrastructure")', [grandparent_lat, grandparent_lon])
        # if cursor.fetchone() != None:
        #     grandparent_id = int(cursor.fetchone())
        #     print grandparent_id
        # else:
        #     grandparent_id = 0
    except Exception as e:
        print("Error handling line No %d. lat = %s, lon = %s" % (count, lat, lon))
        print("mass = %s, type = %s" % (mass, building_type))
        print("address = \n%s\ntype = \n%s\n" % (mass, building_type))
        continue
    cursor.execute('INSERT INTO waste(type,name,address,mass,lat,lon, data,grandparent_id, parent_id)' \
     'VALUES( %s, %s,%s,"%s","%s","%s", %s, %s, %s)',[building_type, name, address, mass, lat, lon, json_data, grandparent_id, 0])
print "--- there are %s in containers.csv" % count
mydb.commit()
cursor.close()
print "Done"
