__author__ = 'whisper'
from support import get_data_csv, haversine, reverse_transofm_coordinates
from os import listdir
import MySQLdb
import json


def prepare_viz():
    mydb = MySQLdb.connect(host='test.rsoo.ru',
                       user='sergey',
                       passwd='Et1Alr2BrGjYTObM',
                       db='sergey',
                       charset="utf8",
                       use_unicode=True)
    cursor = mydb.cursor()
    json_data = {
        "seasons_intensity":
        {
            "summer":"1.5",
            "automn":"0.5",
            "winter":"0.5",
            "spring":"1.5"
        },
        "density": "123",
        "morphology":
        {
            "morphology_type": "type 1",
            "morphology_id": "32123",
            "composition":
            {
                "paper": "0.12",
                "plastic": "0.2",
                "organic": "0.37",
                "other": "0.05"
            }
        }
    }
    cursor.execute('TRUNCATE TABLE route;')
    json_data = json.dumps(json_data)
    type = "truck"
    result_data = get_data_csv("result_3/result.csv")
    for k in range(0, len(result_data), 1):
        from_point = reverse_transofm_coordinates([result_data[k][0], result_data[k][1]])
        to_point = reverse_transofm_coordinates([result_data[k][3], result_data[k][4]])
        cursor.execute('SELECT id FROM waste WHERE lat=%s lon=%s;', [from_point[0], from_point[1]])
        from_id = cursor.fetchone()
        cursor.execute('SELECT id FROM waste WHERE lat=%s lon=%s;', [to_point[0], to_point[1]])
        to_id = int(cursor.fetchone()[0])
        cursor.execute('SELECT lon, lat FROM waste WHERE id=%s;', from_id)
        from_geo_t = cursor.fetchone()
        from_geo = "%s,%s" % (float(from_geo_t[0]), float(from_geo_t[1]))
        cursor.execute('SELECT lon, lat FROM waste WHERE id=%s;', to_id)
        to_geo_t = cursor.fetchone()
        to_geo = "%s,%s" % (float(to_geo_t[0]), float(to_geo_t[1]))
        length = haversine(float(from_geo_t[0]), float(from_geo_t[1]), float(to_geo_t[0]), float(to_geo_t[1]))
        time = length/50
        cursor.execute('INSERT INTO route(type, from, to, from_geo, to_geo, length, time, data)',
                       [type, from_id, to_id, from_geo, to_geo, length, time, json_data])
    mydb.commit()
    cursor.close()


if __name__ == '__main__':
    prepare_viz()